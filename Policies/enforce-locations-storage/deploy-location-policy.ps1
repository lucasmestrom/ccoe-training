# Set the scope to the correct subscription
Set-AzureRmContext -SubscriptionName "ICC Demo"

# Set the scope to a resource group; may also be a resource, subscription, or management group
$scope = Get-AzResourceGroup -Name 'LMES-training'

# Set the variables with links that contain the JSON files for creating the definition
$policy = 'https://bitbucket.org/lucasmestrom/ccoe-training/raw/bf583123ec705986ab001090ed39f24d3bfd7075/Policies/enforce-locations-storage/policy-enforce-location-storage.json'
$definitionparam = 'https://bitbucket.org/lucasmestrom/ccoe-training/raw/ad47479c68a98b3cc7caf8d2ecb61bf4c6f6216f/Policies/enforce-locations-storage/policy-enforce-location-storage.parameters.json'

# Create the Policy Definition (Subscription scope)
$definition = New-AzPolicyDefinition -Name "enforce-storage-location"  `
-DisplayName "Enforced locations for storage deployment"  `
-description "This policy enables you to restrict the locations your organization can specify when deploying resources."  `
-Policy $policy  `
-Parameter $definitionparam -Mode Indexed

# Set the Policy Parameter (JSON format)
$policyparam = '{ 
    "listOfAllowedLocations": { 
        "value": [ 
            "west europe", 
            "korea south", 
            "korea central" 
        ] 
    } 
}'

# Create the Policy Assignment
New-AzPolicyAssignment -Name 'allowed-locations-storage'  `
-DisplayName 'LMES - Allowed locations storage deployment'  `
-Scope $scope.ResourceId  `
-PolicyDefinition $definition  `
-PolicyParameter $policyparam