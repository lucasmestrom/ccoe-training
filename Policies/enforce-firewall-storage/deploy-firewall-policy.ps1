# Set the scope to the correct subscription
Set-AzureRmContext -SubscriptionName "ICC Demo"
$subscriptionId = (Get-AzContext).Subscription.id

# Set the scope to a resource group; may also be a resource, subscription, or management group
$scope = Get-AzResourceGroup -Name 'LMES-training'

# Set the policy to the link that contains the policy JSON file
$policy = 'https://bitbucket.org/lucasmestrom/ccoe-training/raw/e260a89b289fa004d6cd3ff84df02e07b874da48/Policies/enforce-firewall-storage/policy-firewall-deploy.json'

# Create the Policy Definition (Subscription scope)
$definition = New-AzPolicyDefinition -Name "enforce-firewall-storageaccounts"  `
-DisplayName "Enforce firewall for storageAccount deployments"  `
-description "This policy enables you to restrict the deployment of storage accounts without firewalls enabled."  `
-Policy $policy

# Set name and display name for the policy, will be used again in remediation task
$policyName = 'storage-firewall-enabled'
$policyDisplayName = 'LMES - Requires firewall enabled on storageaccount'

# Create the Policy Assignment, requires a location (takes location of the resourcegroup) and to assign a identity
New-AzPolicyAssignment -Name $policyName  `
-DisplayName $policyDisplayName  `
-Scope $scope.ResourceId  `
-PolicyDefinition $definition  `
-Location $scope.location  `
-AssignIdentity