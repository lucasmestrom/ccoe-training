# Storage product CCoE case

This file contains general information on the contents of this workspace. The workspace contains ARM templates and scripts to deploy a storage product.

## Description

The aim is to share some script/ARM to achieve the following goal: 

One of our enterprise customers asked us to create a storage account product that can be used by different DevOps teams. The customer has the following requirements:

* Ability to deploy multiple storage in one subscription at once
* Only allowed to use West Europe and Korea as a region by implementing policies
* Blob GPv2
* Data is top secret (data is encrypted)
* Public endpoint is used
* LRS will be used
* Access tier will be cool
* Firewall enabled by using policies

Deliverables:

* Product created based on scripts and/or arm templates
* Code follows coding guidelines and naming convention
* Read me file is included that explains the structure of the code and the way to deploy


## Content

This workspace contains the following files and folders:

* File: deploy-storageaccount-arm.azcli
* File: azure-deploy.json
* File: azure-deploy.parameters.json
* Folder: Policies
    * enforce-firewall-storage
        * deploy-firewall-policy.ps1
        * policy-firewall-deploy.json
    * enforce-locations-storage
        * deploy-location-policy.ps1
        * policy-enforce-location-storage.json
        * policy-enforce-location-storage.parameters.json

## How to deploy

For deploying the product, you have to run the following scripts in this order:

1. Deploymnet location policy: Run powershell script from policies/enforce-locations-storage/.. in the cloud shell of azure.
2. Deployment firewall policy: Run powershell script from policies/enforce-firewall-storage/.. in the cloud shell of azure.
3. Deployment storage account: Run bash script from root in the cloud shell of azure.

Should you want to modify the name of the storage account, you have to alter azure-deploy.json. Iteration of naming starts at 1 by default.

